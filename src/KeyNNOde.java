import com.sun.istack.internal.NotNull;

public class KeyNNode<K extends Comparable<K>,V> {
    public final K key;
    public final BPlusNode<K,V> node;
    public KeyNNode(@NotNull K key,@NotNull BPlusNode<K,V> node){
        this.key = key;
        this.node = node;
    }
}
