import com.sun.istack.internal.Nullable;

/**
 * Implements a B+ tree in which the keys  are Strings (with
 * maximum length 60 characters) and the values are integers
 */

public class BPlusTreeString60toInt {
    private static final int sizes = 6;
    private int c = 0;

    private BPlusNode<String, Integer> root = null;

    /**
     * Returns the integer associated with the given key,
     * or null if the key is not in the B+ tree.
     */
    public Integer find(String key) {
        if (root == null) {
            return null;
        }
        if (root.valCount() == 1) {
            if (root.getKey(0).equals(key)) {
                return root.getValue(0);
            } else {
                return null;
            }
        }
        return find(key, root);
    }

    private Integer find(String key, BPlusNode<String, Integer> node) {
        //If node is leaf
        if (node.childCount() == 0) {
            for (int i = 0; i < (node.size()); i++) {
                //If the key is the value we are looking for return
                if (key.equals(node.getKey(i))) {
                    return node.getValue(i);
                }
            }
            return null;
        } else if (key.compareTo(node.getKey(0)) < 0) {
            return find(key, node.getChild(0));
        } else {
            for (int i = 1; i < node.size(); i++) {
                //If we went past the key go into child to find
                if (key.compareTo(node.getKey(i)) < 0) {
                    return find(key, node.getChild(i));
                }
            }
            return find(key, node.getChild(node.childCount() - 1));
        }
    }


    /**
     * Stores the value associated with the key in the B+ tree.
     * If the key is already present, replaces the associated value.
     * If the key is not present, adds the key with the associated value
     *
     * @param value
     * @param key
     * @return whether pair was successfully added.
     */
    public boolean put(String key, int value) {
        c++;
        if (root == null) {
            root = new BPlusNode<>(sizes);
            root.insertKey(key, 0);
            root.insertValue(value, 0);
            return true;
        }

        if (root.valCount() == 1) {
            if (key.compareTo(root.getKey(0)) < 0) {
                root.insertKeyAndValue(key, value, 0);
            } else {
                root.insertKeyAndValue(key, value, 1);
            }
            return true;
        }
        int cc = c;

        KeyNNode<String, Integer> keyNNode = put(key, value, root);

        if (keyNNode != null) {
            BPlusNode<String, Integer> node = new BPlusNode<>(sizes);
            node.insertChild(root, 0);
            node.addKey(keyNNode.key);
            node.insertChild(keyNNode.node, 1);
            root = node;
        }
        return true;
    }

    @Nullable
    private KeyNNode<String, Integer> put(String key, int value, BPlusNode<String, Integer> node) {
        if (node.childCount() == 0) {
            if (node.size() < sizes) {
                int vCount = node.valCount();
                for (int i = 0; i < vCount; i++) {
                    if (node.getKey(i).compareTo(key) > 0) {
                        node.insertKeyAndValue(key, value, i);
                        break;
                    }
                    if (i == node.valCount() - 1) {
                        node.insertKeyAndValue(key, value, node.valCount());
                    }
                }
                return null;
            } else {
                return splitLeaf(key, value, node);
            }
        }


        KeyNNode<String, Integer> ret = null;
        if (!(node.childCount() > 0)) {
            return null;
        }
        int s = node.size();
        for (int i = 0; i < s; i++) {
            if (key.compareTo(node.getKey(i)) < 0) {
                ret = put(key, value, node.getChild(i));
                if (ret == null) return null;
                else return dealWithPromote(ret.key, ret.node, node);
            }
        }
        ret = put(key, value, node.getChild(node.childCount() - 1));
        if (ret == null) return null;
        else return dealWithPromote(ret.key, ret.node, node);
    }

    @Nullable
    private KeyNNode<String, Integer> splitLeaf(String key, int value, BPlusNode<String, Integer> node) {
        //insert key and value into leaf in correct place (spilling over end)
        node.insertKVInPlace(key, value);
        //sibling← create new leaf
        BPlusNode<String,Integer> sibling = new BPlusNode<>(sizes);
        //mid← (node.size+1)/2
        int mid = (node.size())/2;
        //move keys and values from mid … size out of node into sibling.
        for(int i = node.keyCount()-1; i!=mid-1; i--){
            sibling.insertKeyAndValue(node.removeKey(i),node.removeValue(i),0);
        }
        //sibling.next← node.next
        sibling.setNext(node.getNext());
        //node.next← sibling
        node.setNext(sibling);
        //return (sibling.keys[0] , sibling)
        return new KeyNNode<>(sibling.getKey(0),sibling);
    }

    @Nullable
    private KeyNNode<String, Integer> dealWithPromote(String newKey, BPlusNode<String, Integer> rightChild, BPlusNode<String, Integer> node) {
        //if (newKey, rightChild) = null return null
        if (newKey == null || rightChild == null) {
            return null;
        }
        node.insertKeyInPlace(newKey);
        node.insertChildInPlace(rightChild);

        if (node.size() <= sizes) {
            return null;
        }
        //sibling ← create new node
        BPlusNode<String, Integer> sibling = new BPlusNode<>(sizes);
        //mid ← (size/2) +1
        int mid = (node.size() / 2);
        //move node.keys[mid+1… node.size] to sibling.node [1… node.size-mid]
        for(int i = node.keyCount()-1; i>mid; i--){
            sibling.insertKey(node.removeKey(i),0);
        }

        //move node.child[mid … node.size] to sibling.child [0 … node.size-mid]
        for(int i = node.childCount()-1; i>mid; i--){
            sibling.insertChild(node.removeChild(i),0);
        }

        //promoteKey←node.keys[mid],
        //remove node.keys[mid]
        //return (promoteKey, sibling)
        String pKey = node.getKey(mid);
        node.removeKey(mid);
        return new KeyNNode<>(pKey, sibling);
    }

    @Nullable
    public Integer iterFind(String key){
        BPlusNode<String, Integer> current = root;
        while (current != null) {
            if (current.childCount() != 0) {
                current = current.getChild(0);
                continue;
            }

            for (int i = 0; i < current.keyCount(); i++) {
                if (key.equals(current.getKey(i))) {
                    return current.getValue(i);
                }
            }
            current = current.getNext();
        }
        return null;
    }
}
