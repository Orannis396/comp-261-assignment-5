import java.util.LinkedList;
import java.util.List;

public class BPlusNodeOLD<K extends Comparable<K>, V> {
    private List<KeyPair<K, V>> values;
    private List<BPlusNodeOLD<K, V>> children;
    private int size = 0;

    public BPlusNodeOLD() {
        values = new LinkedList<>();
        children = new LinkedList<>();
    }


    public List<KeyPair<K, V>> getValues() {
        return values;
    }

    public List<BPlusNodeOLD<K, V>> getChildren() {
        return children;
    }

    public V find(K key) {
        /*
            if node is a leaf
                for i from 0 to node.size-1 for
                    if key = node.keys[ i ] return if return node.values[ i ]
                return null
            if node is an internal node
                for i from 1 to node.size
                    if key < node.keys[ i ] return Find(key, getNode(node.child[i-1]))
                return Find(key, getNode(child[node.size] ))
         */
        if (children == null) {
            for(int i = 0; i<(size-1);i++){
                if(key.equals(values.get(i).key)){
                    return values.get(i).value;
                }
            }
        } else {
            for (int i = 1; i < size; i++) {
                if (key.compareTo(values.get(i).key) < 0) {
                    return children.get(i-1).find(key);
                }
            }
            return children.get(size).find(key);
        }

        return null;
    }

    public boolean add(K key, V value) {
        return false;
    }
}
