import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class BPlusNode<K extends Comparable<K>, V> {
    private List<K> keys;
    private List<V> values;
    private List<BPlusNode<K, V>> children;
    private BPlusNode<K,V> next;
    private int maxSize;

    public BPlusNode(int size) {
        Class<K> c;
        this.maxSize = size;
        keys = new LinkedList<>();
        values = new LinkedList<>();
        children = new ArrayList<>(size + 1);
    }

    public int insertKVInPlace(K key, V value){
        int s = this.size();
        for (int i = 0; i < s; ++i) {
            if (key.compareTo(this.getKey(i)) < 0){
                this.insertKeyAndValue(key, value, i);
                return i;
            }
            if (i + 1 == s) {
                this.insertKeyAndValue(key, value, this.size());
                return i;
            }
        }
        return -1;
    }

    public int insertKeyInPlace(K key){
        int s = this.size();
        for (int i = 0; i < s; ++i) {
            if (key.compareTo(this.getKey(i)) < 0){
                this.insertKey(key, i);
                return i;
            }
            if (i + 1 == s) {
                this.insertKey(key, this.size());
                return i;
            }
        }
        return -1;
    }

    public int insertChildInPlace(BPlusNode<K, V> child){
        K childKey = child.getKey(child.keyCount()-1);
        int s = this.keyCount();
        for (int i = 0; i < s; ++i) {
            if(childKey.compareTo(this.getKey(i))<0){
                this.insertChild(child,i);
                return i;
            }
            if(i+1==s){
                this.insertChild(child,this.childCount());
                return i;
            }
        }
        if(childKey.compareTo(this.getKey(0))<0){
            this.insertChild(child,0);
            return 0;
        }
        return -1;
    }

    public K getKey(int index) {
        return keys.get(index);
    }

    public void insertKey(K key, int index) {
        keys.add(index, key);
    }

    public void addKey(K key){
        keys.add(key);
    }

    public void setKey(K key, int index) {
        keys.set(index, key);
    }

    public K removeKey(int index) {
        return keys.remove(index);
    }

    public V getValue(int index) {
        return values.get(index);
    }

    public void insertValue(V key, int index) {
        values.add(index, key);
    }

    public void setValue(V key, int index) {
        values.set(index, key);
    }

    public V removeValue(int index) {
        return values.remove(index);
    }

    public BPlusNode<K, V> getChild(int index) {
        return children.get(index);
    }

    public void insertChild(BPlusNode<K, V> child, int index) {
        children.add(index, child);
    }

    public void setChild(BPlusNode<K, V> child, int index) {
        children.set(index, child);
    }

    public void insertKeyAndValue(K key, V value, int index){
        keys.add(index,key);
        values.add(index,value);
    }

    public BPlusNode<K, V> removeChild(int index) {
        return children.remove(index);
    }

    public int childCount() {
        return children.size();
    }

    public int valCount() {
        return values.size();
    }

    public int keyCount() {
        return keys.size();
    }

    public int size() {
        return keys.size();
    }

    public void setNext(BPlusNode<K,V> next){
        this.next = next;
    }

    public BPlusNode<K,V> getNext(){
        return next;
    }
}
