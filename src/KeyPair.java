/**
 * Created by Timothy Gray on 29/05/2015.
 */
public class KeyPair<K,V> {
    public final K key;
    public final V value;

    public KeyPair(K key,V value){
        if(key == null || value == null){
            throw new NullPointerException("Values cannot be null");
        }
        this.key = key;
        this.value = value;
    }

    public V getValue(){
        return value;
    }
}
